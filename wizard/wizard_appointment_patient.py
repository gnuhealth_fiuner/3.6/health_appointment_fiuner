# -*- coding: utf-8 -*-
##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from trytond.model import ModelView
from trytond.wizard import Wizard, StateTransition, StateAction, StateView, Button
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.pyson import PYSONEncoder


__all__ = ['OpenAppointmentPatient']

class OpenAppointmentPatient(Wizard):
    'Create Appointment Evaluation'
    __name__ = 'wizard.gnuhealth.appointment.patient'
  
    start_state = 'appointment_patient'
    appointment_patient = StateAction('health_appointment_fiuner.act_app_patient')

    def do_appointment_patient(self, action):      
        appointment = Transaction().context.get('active_id')
        try:
            app_id = \
                Pool().get('gnuhealth.appointment').browse([appointment])[0]            
        except:
            self.raise_user_error('no_record_selected')
        
        try:
            patient_id = app_id.patient.id
        except:
            self.raise_user_error('no_patient')

        data = {'res_id': [patient_id]}
        
        action['views'].reverse()
        
        return action, data
        
    @classmethod
    def __setup__(cls):
        super(OpenAppointmentPatient, cls).__setup__()
        cls._error_messages.update({
            'no_record_selected':
                'You need to select one Appointment record',
            'no_patient':
                'This appointment has no patient',
        })
